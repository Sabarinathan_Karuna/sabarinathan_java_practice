package streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class SortedPractice {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of names ?");
        int numOfNames = sc.nextInt();
        String[] names = new String[numOfNames];

        System.out.println("Enter the names :");
        for (int i = 0; i < names.length; i++) {
            names[i] = sc.next();
        }

        List<String> namesList = Arrays.asList(names);
        List<String> nameStartsWith = namesList.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());

        System.out.println("Name Sorted  : " + nameStartsWith);
    }
}
