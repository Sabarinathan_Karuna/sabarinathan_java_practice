package streams;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;

class DemoRef {
    public void demoPhantomFunction(){
        System.out.println("This is demo Phantom function");
    }
}

public class PhantomReferencePractice {
    public static void main(String[] args){
        DemoRef ref = new DemoRef();
        ref.demoPhantomFunction();

        ReferenceQueue<DemoRef> refQueue = new ReferenceQueue<DemoRef>();
        PhantomReference<DemoRef> phantomReference = new PhantomReference<DemoRef>(ref,refQueue);

        ref = null;
        ref = phantomReference.get();
        ref.demoPhantomFunction();

    }
}
