package streams;

import java.lang.ref.SoftReference;

class Demo{
    public void demoFunction(){
        System.out.println("This is demo function");
    }
}

public class SoftReferencePractice {
    public static void main(String[] args){
        Demo demoRef = new Demo();
        System.out.println("First time demo functon called");
        demoRef.demoFunction();

        SoftReference<Demo> sf = new SoftReference<Demo>(demoRef);

        System.out.println("demoRef assigned to null and used System.gc()");
        demoRef = null;

        System.gc();

        System.out.println("Object got trough softreference.get() ");
        demoRef = sf.get();
        System.out.println("Second time demo functon called");
        demoRef.demoFunction();

    }
}
