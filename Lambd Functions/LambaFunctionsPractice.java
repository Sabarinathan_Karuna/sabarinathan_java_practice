package streams;

interface Operation{
    //Functional interfface
    void arithmetic(int x,int y);
}

public class LambaFunctionsPractice {
    public static void main(String[] args){
        
        //Interface_name 
        Operation addition = (int x,int y)->{
            System.out.println("Addition of "+x+ " and "+y+ " is : " +(x+y));
        };
        Operation subtraction = (int x,int y)->System.out.println("Subtraction of "+x+ " and "+y+ " is : " +(x-y));
        Operation multiply = (int x,int y)->System.out.println("Multiplication of "+x+ " and "+y+ " is : " +(x*y));
        Operation division = (int x,int y)->System.out.println("Division of "+x+ " and "+y+ " is : " +(x/y));

        addition.arithmetic(5,10);
        multiply.arithmetic(5,10);
        subtraction.arithmetic(5,10);
        division.arithmetic(5,10);
    }
}
