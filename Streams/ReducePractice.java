package streams;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ReducePractice {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("How many numbers ?");
        int countOfNumbers = sc.nextInt();
        Integer[] number = new Integer[countOfNumbers];

        System.out.print("Enter the Numbers to calculate Cube : ");
        for( int i=0 ; i<number.length ; i++ ){
            number[i] = sc.nextInt();
        }

        List<Integer> numberList = Arrays.asList(number);
        int evenSum = numberList.stream().filter(x->x%2==0).reduce(0,(ans,i)-> ans+i);
        int oddSum = numberList.stream().filter(x->x%2!=0).reduce(0,(ans,i)-> ans+i);
        System.out.println("Sum of Even Numbers : "+ evenSum);
        System.out.println("Sum of Odd Numbers : "+ oddSum);
    }
}
