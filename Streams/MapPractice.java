package streams;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class MapPractice {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("How many numbers ?");
        int countOfNumbers = sc.nextInt();
        Integer[] number = new Integer[countOfNumbers];

        System.out.print("Enter the Numbers to calculate Cube : ");
        for( int i=0 ; i<number.length ; i++ ){
            number[i] = sc.nextInt();
        }

        List<Integer> numberList = Arrays.asList(number);
        //To store the cube values in the list
        List<Integer> cubes = numberList.stream().map(x -> x * x * x).collect(Collectors.toList());
        System.out.print("Cubes of number : "+cubes);

        //To directly print the cubes using forEach
        numberList.stream().map(x-> x * x).forEach(y->System.out.println(y));
    }
}
