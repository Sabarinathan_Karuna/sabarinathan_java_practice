package streams;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class FilterPractice {
    public static void main(String[] args){

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of names ?");
        int numOfNames = sc.nextInt();
        String[] names = new String[numOfNames];

        System.out.println("Enter the names :");
        for(int i=0;i<names.length;i++){
            names[i] = sc.next();
        }

        System.out.println("Letter with you want to search starts-with");
        String characterToStartsWith = sc.next();

        List<String> namesList = Arrays.asList(names);
        List<String> nameStartsWith = namesList.stream().filter(s->s.startsWith(characterToStartsWith)).collect(Collectors.toList());

        System.out.println("Name starts with : "+ nameStartsWith );

    }

}
